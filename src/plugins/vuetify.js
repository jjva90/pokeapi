import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#d00a41",
        secondary: "#f1c43a",
        terciary: "#3a57a1",
        accent: "#8c9eff",
        correct: "#04a523",
        error: "#b71c1c",
        fire: "#fda5a5",
        grass: "#a3da88",
        electric: "#f8df30",
        water: "#a9bff3",
        ground: "#e0c068",
        rock: "#b8a038",
        fairy: "#efa8ef",
        poison: "#9458ad",
        bug: "#a8b820",
        dragon: "#7038f8",
        psychic: "#f85888",
        flying: "#a890f0",
        fighting: "#c03028",
        ice: "#98d8d8",
        ghost: "#705898",
        normal: "#bfbfa2",
        dark: "#705848",
        steel: "#b8b8d0",
      },
    },
  },
});
